package ${strPackage}.controller;

import ${strPackage}.${tableModel.strEntityNameFirstUpper}Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ${tableModel.strComment?default("")} HTML控制器
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
@Slf4j
@Controller
@RequestMapping(value = ${tableModel.strEntityNameFirstUpper}Constants.TABLE_ENTITY)
public class ${tableModel.strEntityNameFirstUpper}Controller {

}
