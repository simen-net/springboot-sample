package ${strPackage}.controller;

import ${strPackage}.${tableModel.strEntityNameFirstUpper}Constants;
import ${strPackage}.service.${tableModel.strEntityNameFirstUpper}Service;
import ${strPackage}.jpa.${tableModel.strEntityNameFirstUpper}DO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}CreateDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}RetrieveDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}UpdateDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}VO;
import com.strong.utils.JSON;
import com.strong.utils.mvc.ModuleJsonController;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.strong.utils.mvc.MvcConstants.PARAMETER_INTS_ID;
import static com.strong.utils.mvc.MvcConstants.PARAMETER_INT_ID;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_GENERAL;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_SUPER;

/**
 * ${tableModel.strComment?default("")} JSON控制器
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Slf4j
@RestController
@RequestMapping(value = ${tableModel.strEntityNameFirstUpper}Constants.TABLE_ENTITY)
public class ${tableModel.strEntityNameFirstUpper}JsonController implements ModuleJsonController<${tableModel.strEntityNameFirstUpper}VO, ${tableModel.strEntityNameFirstUpper}CreateDTO, ${tableModel.strEntityNameFirstUpper}UpdateDTO, ${tableModel.strEntityNameFirstUpper}RetrieveDTO> {

    /**
     * 过滤排序条件
     */
    private final static String[] STRS_FILTER_SORT = {};

    /**
     * 过滤查询条件
     */
    private final static String[] STRS_FILTER_SEARCH = {};

    /**
     * 注入Service
     */
    private final ${tableModel.strEntityNameFirstUpper}Service ${tableModel.strEntityName}Service;

    /**
     * 实例化
     *
     * @param ${tableModel.strEntityName}Service 注入的Service
     */
    @Autowired
    public ${tableModel.strEntityNameFirstUpper}JsonController(${tableModel.strEntityNameFirstUpper}Service ${tableModel.strEntityName}Service) {
        this.${tableModel.strEntityName}Service = ${tableModel.strEntityName}Service;
    }

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVO<${tableModel.strEntityNameFirstUpper}VO> getRecordView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO = ${tableModel.strEntityName}Service.getRecord(intId);
        ${tableModel.strEntityNameFirstUpper}VO ${tableModel.strEntityName}VO = ${tableModel.strEntityName}Service.getModel(${tableModel.strEntityName}DO);
        return new ReplyVO<>(${tableModel.strEntityName}VO);
    }

    /**
     * 添加记录操作 控制器
     *
     * @param modeldCreate 待添加的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<${tableModel.strEntityNameFirstUpper}VO> getCreateAction(@Valid @RequestBody final ${tableModel.strEntityNameFirstUpper}CreateDTO modeldCreate) {
        ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO = ${tableModel.strEntityName}Service.getCreateAction(modeldCreate);
        ${tableModel.strEntityNameFirstUpper}VO ${tableModel.strEntityName}VO = ${tableModel.strEntityName}Service.getModel(${tableModel.strEntityName}DO, ${tableModel.strEntityNameFirstUpper}VO.STRS_INCLUDE_PROPERTIES);
        System.out.println(JSON.toJSONString(${tableModel.strEntityName}VO));
        return new ReplyVO<>(${tableModel.strEntityName}VO, ReplyEnum.SUCCESS_CREATE_DATA);
    }

    /**
     * 执行修改的 控制器.
     *
     * @param modelUpdate 待修改的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<${tableModel.strEntityNameFirstUpper}VO> getUpdateAction(@Valid @RequestBody final ${tableModel.strEntityNameFirstUpper}UpdateDTO modelUpdate) {
        ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO = ${tableModel.strEntityName}Service.getUpdateAction(modelUpdate);
        ${tableModel.strEntityNameFirstUpper}VO ${tableModel.strEntityName}VO = ${tableModel.strEntityName}Service.getModel(${tableModel.strEntityName}DO, ${tableModel.strEntityNameFirstUpper}VO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(${tableModel.strEntityName}VO, ReplyEnum.SUCCESS_UPDATE_DATA);
    }

    /**
     * 显示待修改内容的 控制器.
     *
     * @param intId 显示对象的ID
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<${tableModel.strEntityNameFirstUpper}VO> getUpdateView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO = ${tableModel.strEntityName}Service.getUpdateRecord(intId);
        ${tableModel.strEntityNameFirstUpper}VO ${tableModel.strEntityName}VO = ${tableModel.strEntityName}Service.getModel(${tableModel.strEntityName}DO);
        return new ReplyVO<>(${tableModel.strEntityName}VO);
    }

    /**
     * 执行删除的 控制器.
     *
     * @param intsId 待删除id数组
     * @return 基于无数据的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId) {
        ${tableModel.strEntityName}Service.getDeleteAction(intsId);
        return new ReplyVO<>(ReplyEnum.SUCCESS_DELETE_DATA);
    }

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型队列的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<${tableModel.strEntityNameFirstUpper}VO> getListView(@RequestBody final ${tableModel.strEntityNameFirstUpper}RetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        List<${tableModel.strEntityNameFirstUpper}DO> list${tableModel.strEntityNameFirstUpper}DO = ${tableModel.strEntityName}Service.getAllList(modelRetrieve);
        List<${tableModel.strEntityNameFirstUpper}VO> list${tableModel.strEntityNameFirstUpper}VO = ${tableModel.strEntityName}Service.getModelList(list${tableModel.strEntityNameFirstUpper}DO, ${tableModel.strEntityNameFirstUpper}VO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(list${tableModel.strEntityNameFirstUpper}VO);
    }

    /**
     * 显示分页列表的 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型分页对象的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<${tableModel.strEntityNameFirstUpper}VO> getPageView(@RequestBody final ${tableModel.strEntityNameFirstUpper}RetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        Page<${tableModel.strEntityNameFirstUpper}DO> page${tableModel.strEntityNameFirstUpper}DO = ${tableModel.strEntityName}Service.getPageList(modelRetrieve);
        List<${tableModel.strEntityNameFirstUpper}VO> list${tableModel.strEntityNameFirstUpper}VO = ${tableModel.strEntityName}Service.getModelList(page${tableModel.strEntityNameFirstUpper}DO.getContent(), ${tableModel.strEntityNameFirstUpper}VO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(list${tableModel.strEntityNameFirstUpper}VO, page${tableModel.strEntityNameFirstUpper}DO.getTotalElements());
    }


    // ===================扩展=======================
}
