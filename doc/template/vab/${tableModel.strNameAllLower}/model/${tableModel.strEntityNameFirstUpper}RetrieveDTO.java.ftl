package ${strPackage}.model;

import lombok.*;
import cn.hutool.core.date.DatePattern;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import com.strong.utils.mvc.pojo.retrieve.VabDTO;

/**
 * ${tableModel.strComment?default("")} 查询模型类
 *
 * @author ${strAuthor}
 * @date ${.now?string("yyyy-MM-dd HH:mm:ss")}
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ${tableModel.strEntityNameFirstUpper}RetrieveDTO extends VabDTO<${tableModel.strEntityNameFirstUpper}VO> {

}