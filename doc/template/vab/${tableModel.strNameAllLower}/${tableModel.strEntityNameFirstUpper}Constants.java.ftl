package ${strPackage};

/**
 * ${tableModel.strComment?default("")} 常量类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public class ${tableModel.strEntityNameFirstUpper}Constants {

    /**
     * ${tableModel.strComment?default("")} 数据库表备注
     */
    public final static String TABLE_COMMENT = "${tableModel.strComment?default("")}";

    /**
     * ${tableModel.strComment?default("")} 数据库表名
     */
    public final static String TABLE_NAME = "${tableModel.strName}";

    /**
     * ${tableModel.strComment?default("")} 表实体名
     */
    public final static String TABLE_ENTITY = "${tableModel.strEntityName}";

    /**
     * ${tableModel.strComment?default("")} 表实体类模块名
     */
    public final static String TABLE_PACKAGE = "${tableModel.strNameAllLower}";

<#-- 遍历表模型中的字段队里生成常量 -->
<#list tableModel.listColumnModel as columnModel>
    /**
    * ${columnModel.strComment?default("")} 字段备注
    */
    public final static String ${columnModel.strConstantsName}_COMMENT = "${columnModel.strComment?default("")}";

    /**
    * ${columnModel.strComment?default("")} 数据库字段名
    */
    public final static String ${columnModel.strConstantsName}_COLUMN = "${columnModel.strName}";

    /**
    * ${columnModel.strComment?default("")} 字段实体名
    */
    public final static String ${columnModel.strConstantsName}_ENTITY = "${columnModel.strEntityName}";

</#list>

// ===================扩展=======================
}
