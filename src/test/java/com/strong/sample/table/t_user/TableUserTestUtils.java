package com.strong.sample.table.t_user;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.Faker;
import com.strong.faker.*;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;
import com.strong.sample.table.t_user.model.TableUserCreateDTO;
import com.strong.utils.constants.RegexConstants;
import com.strong.utils.enum_.IdTypeEnum;

import java.util.List;

import static com.strong.utils.constants.RegexConstants.*;
import static com.strong.utils.security.AuthorityConstants.STRS_AUTHORITY;
import static com.strong.utils.security.AuthorityConstants.STR_AUTHORITY_SUPER;

/**
 * 表用户测试工具
 *
 * @author simen
 * @date 2024/10/29
 */
public class TableUserTestUtils {

    private static final MobilePhoneFaker FAKER_MOBILE_PHONE = Faker.mobilePhone();
    private static final EmailFaker FAKER_EMAIL = Faker.email();
    private static final TelephoneFaker FAKER_TELEPHONE = Faker.telephone();
    private static final IdNumberFaker FAKER_ID_NUMBER = Faker.idNumber();


    /**
     * 获取创建对象的DTO
     *
     * @param listTableUnitDO 单位对象队列
     * @return {@link TableUserCreateDTO }
     */
    public static TableUserCreateDTO getRandomCreateDTO(List<TableUnitDO> listTableUnitDO) {
        String strLoginName = RandomUtil.randomString(CHAR_USER_LOGIN_NAME, RandomUtil.randomInt(6, 16));
        while (!Validator.isMatchRegex(RegexConstants.REGEX_USER_LOGIN_NAME, strLoginName)) {
            strLoginName = RandomUtil.randomString(CHAR_USER_LOGIN_NAME, RandomUtil.randomInt(8, 33));
        }

        String strPwd = RandomUtil.randomString(CHAR_LETTERS_AND_NUMBERS, RandomUtil.randomInt(8, 33));
        while (!Validator.isMatchRegex(RegexConstants.REGEX_LETTERS_AND_NUMBERS, strPwd)) {
            strPwd = RandomUtil.randomString(CHAR_LETTERS_AND_NUMBERS, RandomUtil.randomInt(8, 33));
        }

        String strName = Faker.personName().single().getFullName();
        String strSalt = RandomUtil.randomString(32);
        String strAuthority;
        if (RandomUtil.randomBoolean()) {
            strAuthority = StrUtil.join(",", (Object) STRS_AUTHORITY);
        } else {
            strAuthority = RandomUtil.randomEle(STRS_AUTHORITY);
        }

        return getCreateDTO(listTableUnitDO, strName, strLoginName, strPwd, strSalt, strAuthority,
                FAKER_MOBILE_PHONE.single(), FAKER_TELEPHONE.single(), FAKER_EMAIL.single());
    }

    /**
     * 获取创建对象的DTO
     *
     * @param listTableUnitDO 单位对象队列
     * @param strName         名字
     * @param strLoginName    登录名
     * @param strPwd          密码
     * @param strSalt         密码盐
     * @return {@link TableUserCreateDTO }
     */
    public static TableUserCreateDTO getCreateDTO(List<TableUnitDO> listTableUnitDO,
                                                  String strName, String strLoginName, String strPwd, String strSalt) {
        return getCreateDTO(listTableUnitDO, strName, strLoginName, strPwd, strSalt, STR_AUTHORITY_SUPER,
                FAKER_MOBILE_PHONE.single(), FAKER_TELEPHONE.single(), FAKER_EMAIL.single());
    }

    /**
     * 获取创建对象的DTO
     *
     * @param listTableUnitDO 单位对象队列
     * @param strName         名字
     * @param strLoginName    登录名
     * @param strPwd          密码
     * @param strSalt         密码盐
     * @param strMobilePhone  手机
     * @param strTelephone    固话
     * @param strEmail        电子邮件
     * @return {@link TableUserCreateDTO }
     */
    public static TableUserCreateDTO getCreateDTO(List<TableUnitDO> listTableUnitDO,
                                                  String strName, String strLoginName, String strPwd, String strSalt,
                                                  String strAuthority, String strMobilePhone, String strTelephone, String strEmail) {
        TableUserCreateDTO model = new TableUserCreateDTO();
        model.setUserName(strName);
        model.setUserLoginName(strLoginName);
        model.setUserLoginPassword(strPwd);
        model.setUserLoginSalt(strSalt);
        model.setUserAuthority(strAuthority);
        model.setUserGroup("UserGroup");
        model.setUserTeamOrgId("TeamOrgId");
        model.setUserUnitId(listTableUnitDO.get(RandomUtil.randomInt(0, listTableUnitDO.size() - 1)).getUnitId());

        model.setUserIdType(RandomUtil.randomEle(IdTypeEnum.values()).getCode());
        if (model.getUserIdType().equals(IdTypeEnum.IDENTIFICATION_CARD.getName())) {
            model.setUserIdNumber(FAKER_ID_NUMBER.single().getIdNumber());
        } else if (model.getUserIdType().equals(IdTypeEnum.HONG_KONG_MACAO_PERMIT.getName())) {
            model.setUserIdNumber(CommonFaker.randomHongKongOrMacaoPermitId());
        } else if (model.getUserIdType().equals(IdTypeEnum.TAIWAN_PERMIT.getName())) {
            model.setUserIdNumber(CommonFaker.randomTaiWanPermitId());
        } else if (model.getUserIdType().equals(IdTypeEnum.FOREIGN_PASSPORT.getName())) {
            model.setUserIdNumber(CommonFaker.randomForeignPassportId());
        }

        model.setUserMobilePhone(strMobilePhone);
        model.setUserTelephone(strTelephone);
        model.setUserEmail(strEmail);
        model.setUserQqCode(String.valueOf(RandomUtil.randomInt(1000000, 99999999)));
        model.setUserWxCode(RandomUtil.randomString(CHAR_LETTERS_LOWER_AND_UPPER, 1) + RandomUtil.randomString(CHAR_USER_LOGIN_NAME, RandomUtil.randomInt(5, 20)));
        return model;
    }
}
