package com.strong.sample.table.t_user;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.RandomUtil;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_unit.jpa.TableUnitDAO;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;
import com.strong.sample.table.t_user.jpa.TableUserDAO;
import com.strong.sample.table.t_user.jpa.TableUserDO;
import com.strong.sample.table.t_user.model.TableUserCreateDTO;
import com.strong.sample.table.t_user.model.TableUserVO;
import com.strong.sample.table.t_user.service.TableUserService;
import com.strong.sample.utils.MockMvcUtils;
import com.strong.utils.enum_.IdTypeEnum;
import com.strong.utils.mvc.cache.CacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.strong.config.GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION;
import static com.strong.sample.table.t_user.TableUserConstants.*;
import static com.strong.sample.table.t_user.TableUserJsonControllerTest.STR_URL_CREATE_ACTION;
import static com.strong.sample.table.t_user.TableUserTestUtils.getRandomCreateDTO;
import static com.strong.utils.constants.MessageConstants.*;
import static com.strong.utils.constants.RegexConstants.*;
import static com.strong.utils.security.jwt.JwtTokenUtils.MSG_SIGNATURE_FAILED;

@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableUserTest extends SpringbootTests {

    private static final Integer INT_TEST_COUNT = 1000;

    /**
     * 模拟mvc对象
     */
    @Autowired
    public MockMvc mockMvc;

    /**
     * 模拟mvc工具对象
     */
    @Autowired
    public MockMvcUtils mockMvcUtils;

    /**
     * 数据库操作类
     */
    @Autowired
    TableUserDAO tableUserDAO;

    /**
     * 数据库操作类
     */
    @Autowired
    TableUnitDAO tableUnitDAO;

    /**
     * Service
     */
    @Autowired
    TableUserService tableUserService;

    /**
     * 缓存工具类
     */
    @Autowired
    CacheUtils cacheUtils;

    @Test
    @Order(1)
    @DisplayName("清空记录")
    @Tag("FEATURE_TEST")
    public void clearRecord() {
        tableUserDAO.deleteAllInBatch();
    }

    @Test
    @Order(2)
    @DisplayName("初始化超级管理员用户数据")
    @Tag("FEATURE_TEST")
    public void testInitializeSuperAdministrator() {
        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAll();

        String strName = "胡维";
        String strLoginName = "simen_net";
        String strPwd = "wearenet1234";
        String strSalt = RandomUtil.randomString(32);

        tableUserDAO.deleteByUserLoginName(strLoginName);

        TableUserCreateDTO tableUserCreateDTO = TableUserTestUtils.getCreateDTO(listTableUnitDO, strName, strLoginName, strPwd, strSalt);
        tableUserService.getCreateAction(tableUserCreateDTO);
    }

    @Test
    @Order(3)
    @DisplayName("初始化普通管理员用户数据")
    @Tag("FEATURE_TEST")
    public void testInitializeAdministrator() throws Exception {
        postLogin();

        // 获取单位对象队列
        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAll();

        // 记录成功次数
        int intSuccessCount = 0;
        // 循环添加用户
        for (int i = 0; i < INT_TEST_COUNT; i++) {
            // 获取随机添加对象
            TableUserCreateDTO tableUserCreateDTO = getRandomCreateDTO(listTableUnitDO);

            // 判断登录名是否重复
            if (tableUserDAO.getCountByLoginName(tableUserCreateDTO.getUserLoginName()) == 0) {
                TableUserVO tableUserVO = mockMvcUtils.postReturnReplyVO(STR_URL_CREATE_ACTION, STR_TOKEN_ADMIN, tableUserCreateDTO, TableUserVO.class);
                Assert.isTrue(tableUserDAO.getCountByLoginName(tableUserVO.getUserLoginName()) == 1,
                        "添加【{}】失败", tableUserVO.getUserLoginName());
                intSuccessCount++;
            }
        }
        log.info("成功添加测试记录【{}】条", intSuccessCount);
    }

    @Test
    @Order(80)
    @DisplayName("权限异常测试")
    @Tag("FEATURE_TEST")
    public void testPermissionException() throws Exception {
        postLogin();

        // 获取单位对象队列
        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAll();

        // 循环测试错误签名，断言错误
        for (int i = 0; i < INT_TEST_COUNT; i++) {
            TableUserCreateDTO tableUserCreateDTO = TableUserTestUtils.getRandomCreateDTO(listTableUnitDO);
            mockMvcUtils.postAssertException(STR_URL_CREATE_ACTION, RandomUtil.randomString(8), tableUserCreateDTO, MSG_SIGNATURE_FAILED);
        }

        // 循环测试无权限，断言错误
        String[] strsAuthority = {"", null, STR_TOKEN_USER};
        for (String strAuthority : strsAuthority) {
            TableUserCreateDTO tableUserCreateDTO = getRandomCreateDTO(listTableUnitDO);
            mockMvcUtils.postAssertException(STR_URL_CREATE_ACTION, strAuthority, tableUserCreateDTO, MSG_ACCESS_DENIED_EXCEPTION);
        }
    }

    @Test
    @Order(98)
    @DisplayName("参数数据异常测试")
    @Tag("FEATURE_TEST")
    public void testArgumentDataError() throws Exception {
        postLogin();

        // 获取单位对象队列
        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAll();

        Map<String, String> mapError = new HashMap<>();
        mapError.put(USER_LOGIN_PASSWORD_ENTITY, ERROR_PASSWORD_NO_STANDARD);
        List<String> listErrorPWD = new ArrayList<>();
        for (int i = 0; i < INT_TEST_COUNT; i++) {
            listErrorPWD.add(RandomUtil.randomString(CHAR_LETTERS_LOWER_AND_UPPER, RandomUtil.randomInt(8, 33)));
            listErrorPWD.add(RandomUtil.randomString(CHAR_NUMBERS, RandomUtil.randomInt(8, 33)));
            listErrorPWD.add(RandomUtil.randomString(CHAR_LETTERS_AND_NUMBERS, RandomUtil.randomInt(4, 8)));
            listErrorPWD.add(RandomUtil.randomString(CHAR_LETTERS_AND_NUMBERS, RandomUtil.randomInt(33, 64)));
            listErrorPWD.add(RandomUtil.randomString(CHAR_LETTERS_AND_NUMBERS, RandomUtil.randomInt(4, 8)) +
                    RandomUtil.randomString(CHAR_SPECIAL_CHARACTERS, RandomUtil.randomInt(4, 24)));
        }
        for (String strPassword : listErrorPWD) {
            TableUserCreateDTO tableUserCreateDTO = getRandomCreateDTO(listTableUnitDO);
            tableUserCreateDTO.setUserLoginPassword(strPassword);
            mockMvcUtils.postAssertException(STR_URL_CREATE_ACTION, STR_TOKEN_ADMIN, tableUserCreateDTO, mapError);
        }
    }

    @Test
    @Order(99)
    @DisplayName("数据逻辑异常测试")
    @Tag("FEATURE_TEST")
    public void testLogicDataError() throws Exception {
        postLogin();

        // 获取单位对象队列
        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAll();
        // 获取用户对象队列
        List<TableUserDO> listTableUserDO = tableUserDAO.findAll();

        Map<String, String> mapError = new HashMap<>();
        mapError.put(USER_LOGIN_NAME_ENTITY, ERROR_LOGIN_NAME_EXISTS);
        mapError.put(USER_ID_NUMBER_ENTITY, ERROR_ID_NUMBER_NO_STANDARD);
        mapError.put(USER_UNIT_ID_ENTITY, ERROR_UNIT_ERROR);
        mapError.put(USER_AUTHORITY_ENTITY, ERROR_AUTHORITY_FORMAT);
        // 循环测试添加不符合证件格式，断言错误
        for (int i = 0; i < INT_TEST_COUNT; i++) {
            TableUserCreateDTO tableUserCreateDTO = getRandomCreateDTO(listTableUnitDO);
            tableUserCreateDTO.setUserLoginName(RandomUtil.randomEle(listTableUserDO).getUserLoginName());
            tableUserCreateDTO.setUserIdType(RandomUtil.randomEle(IdTypeEnum.values()).getCode());
            tableUserCreateDTO.setUserIdNumber(RandomUtil.randomString(32));
            tableUserCreateDTO.setUserUnitId(RandomUtil.randomInt(1, 9999));
            tableUserCreateDTO.setUserAuthority(RandomUtil.randomString(32));
            mockMvcUtils.postAssertException(STR_URL_CREATE_ACTION, STR_TOKEN_ADMIN, tableUserCreateDTO, mapError);
        }
    }

}