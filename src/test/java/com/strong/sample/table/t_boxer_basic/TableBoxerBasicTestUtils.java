package com.strong.sample.table.t_boxer_basic;

import cn.hutool.core.util.RandomUtil;
import com.strong.Faker;
import com.strong.faker.*;
import com.strong.model.IdNumber;
import com.strong.model.Person;
import com.strong.sample.table.t_boxer_basic.enum_.*;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicCreateDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicUpdateDTO;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;

import java.util.List;

public class TableBoxerBasicTestUtils {

    private static final MobilePhoneFaker FAKER_MOBILE_PHONE = Faker.mobilePhone();
    private static final EmailFaker FAKER_EMAIL = Faker.email();
    private static final TelephoneFaker FAKER_TELEPHONE = Faker.telephone();
    private static final IdNumberFaker FAKER_ID_NUMBER = Faker.idNumber();
    private static final PersonFaker FAKER_PERSON = Faker.person();
    private static final PlaceFaker FAKER_PLACE = Faker.place();

    public static TableBoxerBasicCreateDTO getCreateDTO(List<TableUnitDO> listTableUnitDO) {
        TableBoxerBasicCreateDTO model = new TableBoxerBasicCreateDTO();
        Person person = FAKER_PERSON.single();
        IdNumber idNumber = person.getIdNumber();

        model.setBoxerAthleteId(RandomUtil.randomString(32));
        model.setBoxerName(person.getPersonName().getFullName());
        model.setBoxerLocalRegisterInfoId(RandomUtil.randomString(32));
        model.setBoxerUnitId(listTableUnitDO.get(RandomUtil.randomInt(0, listTableUnitDO.size() - 1)).getUnitId());
        model.setBoxerIdNumber(idNumber.getIdNumber());
        model.setBoxerNativePlace(idNumber.getPlace().getAddressDetail());
        model.setBoxerEmail(person.getEmail());
        model.setBoxerMobile(person.getMobilePhone());
        model.setBoxerHasPhoto(RandomUtil.randomBoolean());

        model.setBoxerNationality(RandomUtil.randomEle(BoxerNationalityEnum.values()).getCode());
        model.setBoxerEducation(RandomUtil.randomEle(BoxerEducationEnum.values()).getCode());
        model.setBoxerHealth(RandomUtil.randomEle(BoxerHealthEnum.values()).getCode());
        model.setBoxerStatus(RandomUtil.randomEle(BoxerStatusEnum.values()).getCode());
        model.setBoxerGrade(RandomUtil.randomEle(BoxerGradeEnum.values()).getCode());
        model.setBoxerType(RandomUtil.randomEle(BoxerTypeEnum.values()).getCode());

        if (RandomUtil.randomBoolean()) {
            model.setBoxerGuardianIdNumber(person.getIdNumber().getIdNumber());
            model.setBoxerGuardianMobile(FAKER_MOBILE_PHONE.single());
            model.setBoxerGuardianName(person.getPersonName().getFullName());
            model.setBoxerGuardianRelation(RandomUtil.randomEle(BoxerRelationEnum.values()).getValue());
        }

        return model;
    }

    public static TableBoxerBasicUpdateDTO getUpdateDTO(List<TableUnitDO> listTableUnitDO) {
        TableBoxerBasicUpdateDTO model = new TableBoxerBasicUpdateDTO();
        Person person = FAKER_PERSON.single();
        IdNumber idNumber = person.getIdNumber();

        model.setBoxerAthleteId(RandomUtil.randomString(32));
        model.setBoxerName(person.getPersonName().getFullName());
        model.setBoxerLocalRegisterInfoId(RandomUtil.randomString(32));
        model.setBoxerUnitId(listTableUnitDO.get(RandomUtil.randomInt(0, listTableUnitDO.size() - 1)).getUnitId());
        model.setBoxerIdNumber(idNumber.getIdNumber());
        model.setBoxerNativePlace(idNumber.getPlace().getAddressDetail());
        model.setBoxerEmail(person.getEmail());
        model.setBoxerMobile(person.getMobilePhone());
        model.setBoxerHasPhoto(RandomUtil.randomBoolean());

        model.setBoxerNationality(RandomUtil.randomEle(BoxerNationalityEnum.values()).getCode());
        model.setBoxerEducation(RandomUtil.randomEle(BoxerEducationEnum.values()).getCode());
        model.setBoxerHealth(RandomUtil.randomEle(BoxerHealthEnum.values()).getCode());
        model.setBoxerStatus(RandomUtil.randomEle(BoxerStatusEnum.values()).getCode());
        model.setBoxerGrade(RandomUtil.randomEle(BoxerGradeEnum.values()).getCode());
        model.setBoxerType(RandomUtil.randomEle(BoxerTypeEnum.values()).getCode());

        if (RandomUtil.randomBoolean()) {
            model.setBoxerGuardianIdNumber(person.getIdNumber().getIdNumber());
            model.setBoxerGuardianMobile(FAKER_MOBILE_PHONE.single());
            model.setBoxerGuardianName(person.getPersonName().getFullName());
            model.setBoxerGuardianRelation(RandomUtil.randomEle(BoxerRelationEnum.values()).getValue());
        }

        return model;
    }

}
