package com.strong.sample.table.t_test;

import cn.hutool.core.date.DateField;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.Faker;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_test.jpa.TableTestDAO;
import com.strong.sample.table.t_test.jpa.TableTestDO;
import com.strong.sample.table.t_test.model.TableTestCreateDTO;
import com.strong.sample.table.t_test.model.TableTestRetrieveDTO;
import com.strong.sample.table.t_test.model.TableTestUpdateDTO;
import com.strong.sample.table.t_test.service.TableTestService;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.cache.CacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 测试表Service测试
 *
 * @author simen
 * @date 2024/11/19
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableTestServiceTest extends SpringbootTests {

    /**
     * 数据库操作类
     */
    @Autowired
    TableTestDAO tableTestDAO;

    /**
     * Service
     */
    @Autowired
    TableTestService tableTestService;

    /**
     * 缓存工具类
     */
    @Autowired
    CacheUtils cacheUtils;

    /**
     * 拟测试记录数量
     */
    private static final Integer INT_RECORD_COUNT = RandomUtil.randomInt(100, 999);

    /**
     * 循环次数
     */
    private static final Integer INT_LOOP_COUNT = RandomUtil.randomInt(10, 99);

    /**
     * 主键ID字段名
     */
    private static final String STR_ID_FILED_NAME = TableTestConstants.TEST_ID_ENTITY;

    /**
     * 测试用文本字段名
     */
    private static final String STR_TEXT_FILED_NAME = TableTestConstants.TEST_STR_ENTITY;

    static {
        Faker.initialize();
    }

    @Test
    @Order(1)
    @DisplayName("测试添加数据")
    @Tag("BASIC_TEST")
    public void testAddData() {
        // 清空所有记录
        tableTestDAO.deleteAllInBatch();

        // 循环添加记录
        for (int i = 0; i < INT_RECORD_COUNT; i++) {
            TableTestCreateDTO modelCreate = getRandomCreateDTO();
            tableTestService.getCreateAction(modelCreate);
        }
    }

    @Test
    @Order(2)
    @DisplayName("测试条件查询所有记录")
    @Tag("BASIC_TEST")
    public void testQueryAll() {
        // 清空缓存
        tableTestService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        String strLike = "a";

        // 构建查询条件
        TableTestRetrieveDTO tableTestRetrieveDTO = getTableTestRetrieveDTO();
        tableTestRetrieveDTO.addSearch(STR_TEXT_FILED_NAME, String.format("%%%s%%", strLike));

        // 根据查询条件获取所有记录
        List<TableTestDO> listTableTestDO = tableTestService.getAllList(tableTestRetrieveDTO);
        for (TableTestDO tableTestDO : listTableTestDO) {
            String strTextValue = getTextValue(tableTestDO);
            Assert.isTrue(StrUtil.contains(strTextValue, strLike),
                    "查询记录【{}】不匹配【{}】：", strTextValue, strLike);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableTestConstants.TABLE_ENTITY, "getAllList", listTableTestDO.size());
    }

    @Test
    @Order(3)
    @DisplayName("测试分页查询")
    @Tag("BASIC_TEST")
    public void testQueryPage() {
        // 清空缓存
        tableTestService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        // 构建查询条件
        TableTestRetrieveDTO retrievePageDTO = getTableTestRetrieveDTO();
        retrievePageDTO.setIntPageSize(RandomUtil.randomInt(5, 15));

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableTestDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        // 页码总数
        int intPageCount = intActualRecordCount / retrievePageDTO.getIntPageSize();
        // 分页后的余数
        int intPageRemainder = intActualRecordCount % retrievePageDTO.getIntPageSize();
        // 如果分页余数大于0，则页码总数加1
        if (intPageRemainder > 0) {
            intPageCount++;
        }

        // 循环查询分页记录
        for (int i = 1; i <= intPageCount; i++) {
            // 该分页预期的记录数
            Integer intExpectedNumber = retrievePageDTO.getIntPageSize();
            // 如果是否到最后一页，且余数大于0，则预期记录数为分页后的余数
            if (i == intPageCount && intPageRemainder > 0) {
                intExpectedNumber = intPageRemainder;
            }

            // 设置查询页码
            retrievePageDTO.setIntPageNo(i - 1);

            // 查询获取分页对象
            Page<TableTestDO> pageTableTestDO = tableTestService.getPageList(retrievePageDTO);
            Assert.notNull(pageTableTestDO, "返回对象为空");

            // 获取分页队列
            List<TableTestDO> listPageContent = pageTableTestDO.getContent();
            Assert.notEmpty(listPageContent, "返回记录队列为空");

            System.out.printf("总记录数【%d】当前页【%d/%d】本页记录数【预期%d/实际%s】\n",
                    intActualRecordCount, retrievePageDTO.getIntPageNo() + 1,
                    intPageCount, intExpectedNumber, listPageContent.size());

            Assert.isTrue(intActualRecordCount == pageTableTestDO.getTotalElements(),
                    "实际记录数量【{}】与查询到的总记录数【{}】不一致",
                    intActualRecordCount, pageTableTestDO.getTotalElements());
            Assert.equals(listPageContent.size(), intExpectedNumber,
                    "返回的记录数[{}]与页码[{}]不一致", listPageContent.size(), intExpectedNumber);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableTestConstants.TABLE_ENTITY, "getPageList", intActualRecordCount);
    }

    @Test
    @Order(4)
    @DisplayName("测试查询单条记录")
    @Tag("BASIC_TEST")
    public void testQueryRecord() {
        // 清空缓存
        tableTestService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        List<TableTestDO> listTableTestDO = tableTestDAO.findAll();
        // 遍历记录断言每条记录非空
        for (TableTestDO tableTestDO : listTableTestDO) {
            Integer intId = getIdValue(tableTestDO);
            TableTestDO tableTestDORecord = tableTestService.getRecord(intId);
            Assert.notNull(tableTestDORecord, "查询到的记录为空：ID={}", intId);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableTestConstants.TABLE_ENTITY, "getRecord", listTableTestDO.size());
    }

    @Test
    @Order(5)
    @DisplayName("测试修改数据")
    @Tag("BASIC_TEST")
    public void TestUpdateData() {
        // 编修修改所有记录
        TableTestRetrieveDTO tableTestRetrieveDTO = getTableTestRetrieveDTO();
        List<TableTestDO> listTableTestDO = tableTestService.getAllList(tableTestRetrieveDTO);

        for (TableTestDO tableTestDO : listTableTestDO) {
            Integer intId = getIdValue(tableTestDO);

            TableTestUpdateDTO modelUpdate = getRandomUpdateDTO();
            modelUpdate.setTestId(intId);

            TableTestDO updateDO = tableTestService.getUpdateAction(modelUpdate);

            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(modelUpdate, updateDO);
        }
    }

    @Test
    @Order(6)
    @DisplayName("测试删除数据")
    @Tag("BASIC_TEST")
    public void testDeleteData() {
        // 构建查询条件
        TableTestRetrieveDTO tableTestRetrieveDTO = getTableTestRetrieveDTO();
        // 获取所有记录
        List<TableTestDO> listTableTestVO = tableTestService.getAllList(tableTestRetrieveDTO);

        int intSplit = listTableTestVO.size() / 2;
        Integer[] intsId = new Integer[listTableTestVO.size() - intSplit];
        for (int i = 0; i < listTableTestVO.size(); i++) {
            TableTestDO tableTestVO = listTableTestVO.get(i);
            Integer intId = getIdValue(tableTestVO);

            // 遍历记录，前一半逐条删除，后一半合并数组一次性删除
            if (i < intSplit) {
                // 发送删除请求
                tableTestService.getDeleteAction(intId);
                // 根据ID查询DO，断言其为空
                Optional<TableTestDO> optional = tableTestDAO.findById(intId);
                Assert.isTrue(optional.isEmpty(), "已删除的DO对象依旧存在：【{}】", JSON.toJSONString(optional));
            } else {
                intsId[i - intSplit] = intId;
            }
        }

        // 删除剩余记录
        tableTestService.getDeleteAction(intsId);
        Integer intCount = tableTestDAO.getCount();
        Assert.isTrue(intCount == 0, "未完全删除所有记录");
    }

    /**
     * 获取随机 修改dto
     *
     * @return {@link TableTestUpdateDTO }
     */
    public static TableTestUpdateDTO getRandomUpdateDTO() {
        TableTestUpdateDTO model = new TableTestUpdateDTO();
        model.setTestText(RandomUtil.randomString(100));
        model.setTestStr(RandomUtil.randomString(10));
        model.setTestDate(RandomUtil.randomDate(new Date(), DateField.YEAR, 20, 50).toJdkDate());
        model.setTestDatetime(RandomUtil.randomDate(new Date(), DateField.YEAR, 20, 50).toJdkDate());
        model.setTestFloat(RandomUtil.randomFloat());
        return model;
    }

    /**
     * 获取随机 创建dto
     *
     * @return {@link TableTestCreateDTO }
     */
    public static TableTestCreateDTO getRandomCreateDTO() {
        TableTestCreateDTO model = new TableTestCreateDTO();
        model.setTestText(RandomUtil.randomString(100));
        model.setTestStr(RandomUtil.randomString(10));
        model.setTestDate(RandomUtil.randomDate(new Date(), DateField.YEAR, 20, 50).toJdkDate());
        model.setTestDatetime(RandomUtil.randomDate(new Date(), DateField.YEAR, 20, 50).toJdkDate());
        model.setTestFloat(RandomUtil.randomFloat());
        return model;
    }

    /**
     * 获取查询条件
     *
     * @return {@link TableTestRetrieveDTO }
     */
    private static TableTestRetrieveDTO getTableTestRetrieveDTO() {
        // 构建查询条件
        TableTestRetrieveDTO tableTestRetrieveDTO = new TableTestRetrieveDTO();
        tableTestRetrieveDTO.setSort(Sort.by(STR_ID_FILED_NAME));
        return tableTestRetrieveDTO;
    }

    /**
     * 获取主键id内容
     *
     * @param objBean 对象
     * @return {@link Integer }
     */
    private Integer getIdValue(Object objBean) {
        return StrongUtils.getIntByFiledName(objBean, STR_ID_FILED_NAME);
    }

    /**
     * 获取测试用文本字段内容
     *
     * @param objBean 对象
     * @return {@link Integer }
     */
    private String getTextValue(Object objBean) {
        return StrongUtils.getStringByFiledName(objBean, STR_TEXT_FILED_NAME);
    }

}