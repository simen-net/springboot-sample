<!DOCTYPE html>
<html>
<head>
    <title>错误页面</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        * {
            box-sizing: border-box;
            font-family: Arial, sans-serif;
        }

        body {
            margin: 0;
            padding: 0;
            background-color: #f2f2f2;
        }

        .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 80vh;
        }

        h1 {
            font-size: 5rem;
            font-weight: bold;
            color: #333;
            margin-bottom: 1rem;
        }

        p {
            font-size: 2rem;
            color: #666;
            margin-bottom: 4rem;
            text-align: center;
        }

        a {
            display: inline-block;
            padding: 1rem 2rem;
            background-color: #333;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: all 0.3s ease;
        }

        a:hover {
            background-color: #f44336;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>${status}</h1>
    <p>${error}</p>
    <a href="/">返回首页</a>
</div>
</body>
</html>