package com.strong.config.security.userdetails;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

/**
 * SpringSecurity 用户登录信息实现
 *
 * @author simen
 * @date 2022/02/07
 */
@Slf4j
@Data
public class JwtUserDetails implements UserDetails {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 令牌是否待刷新
     */
    private Boolean isToRefresh;

    /**
     * 权限列表
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * 扩展属性map
     */
    private Map<String, Object> mapProperties;

    /**
     * 实例化
     */
    public JwtUserDetails() {
    }

    /**
     * 实例化
     *
     * @param strUserName   用户名
     * @param strPassword   密码
     * @param isToRefresh   令牌是否待刷新
     * @param authorities   权限列表
     * @param mapProperties 扩展属性map
     */
    public JwtUserDetails(String strUserName, String strPassword, Boolean isToRefresh,
                          Collection<? extends GrantedAuthority> authorities,
                          Map<String, Object> mapProperties) {
        this.username = strUserName;
        this.password = strPassword;
        this.isToRefresh = isToRefresh;
        this.authorities = authorities;
        this.mapProperties = mapProperties;
    }

    /**
     * 获取权限队列
     *
     * @return {@link Collection}<{@link ?} {@link extends} {@link GrantedAuthority}>
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * 获得密码
     *
     * @return {@link String}
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * 获得用户名
     *
     * @return {@link String}
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * 账号未过期
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账号未锁定
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 账号证书未过期
     *
     * @return boolean
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账号有效
     *
     * @return boolean
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
