package com.strong.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * 获取当前项目环境： dev、test、prod
 *
 * @author simen
 * @date 2023/03/21
 */
@Configuration
public class ProfileConfig {

    /**
     * 开发环境
     */
    public static final String DEV_PROFILE = "dev";

    /**
     * 测试环境
     */
    public static final String TEST_PROFILE = "test";

    /**
     * 成产环境
     */
    public static final String PROD_PROFILE = "prod";

    /**
     * 应用程序上下文
     */
    private final ApplicationContext context;

    /**
     * 实例化
     *
     * @param context 上下文
     */
    public ProfileConfig(ApplicationContext context) {
        this.context = context;
    }

    /**
     * 获取当前配置的环境
     *
     * @return {@link String}
     */
    public String getActiveProfile() {
        return context.getEnvironment().getActiveProfiles()[0];
    }
}