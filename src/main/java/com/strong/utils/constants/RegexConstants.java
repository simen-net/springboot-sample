package com.strong.utils.constants;

public class RegexConstants {

    /**
     * 正则表达式 用户名（以小写字母开头开头，仅包含小写字母、数字、下划线、减号的6到16位字符串）
     */
    public final static String REGEX_USER_LOGIN_NAME = "^[a-z][a-z0-9_-]{5,15}$";

    /**
     * 正则表达式 只能由字母组成，8-32位
     */
    public final static String REGEX_LETTER = "^[a-zA-Z]{8,32}$";

    /**
     * 正则表达式 只能由数字组成，8-32位
     */
    public final static String REGEX_NUMBERS = "^\\d{8,32}$";

    /**
     * 正则表达式 只能由字母、数字组成，8-32位
     */
    public final static String REGEX_LETTERS_AND_NUMBERS = "^(?=.*\\d)(?=.*[A-z])[\\da-zA-Z]{8,32}$";

    /**
     * 正则表达式 只能由字母、特殊字符组成，8-32位
     */
    public final static String REGEX_LETTERS_AND_SPECIAL_CHARACTERS = "^(?=.*[a-zA-Z])(?=.*[^\\da-zA-Z\\s])([a-zA-Z]|[^\\da-zA-Z\\s]){8,32}$";

    /**
     * 正则表达式 至少包含字母、数字、特殊字符，8-32位
     */
    public final static String REGEX_LETTERS_AND_NUMBERS_AND_SPECIAL_CHARACTERS = "^(?=.*\\d)(?=.*[a-zA-Z])(?=.*[^\\da-zA-Z\\s]).{8,32}$";

    /**
     * 正则表达式 固定电话
     */
    public final static String REGEX_TELEPHONE = "^0\\d{2,3}-?[1-9]?\\d{5,7}$";

    /**
     * 正则表达式 电子邮件
     */
    public final static String REGEX_EMAIL = "^[A-Za-z0-9-_\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";

    /**
     * 正则表达式 QQ号码
     */
    public final static String REGEX_QQ = "^[1-9]{1}[0-9]{4,14}$";

    /**
     * 正则表达式 微信号码
     */
    public final static String REGEX_WX = "^[a-zA-Z][-_a-zA-Z0-9]{5,19}$";

    /**
     * 常用字符 特殊字符
     */
    public final static String CHAR_SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]{}|;':\"<>?,./";

    /**
     * 常用字符 小写字母
     */
    public final static String CHAR_LETTERS_LOWER = "abcdefghijklmnopqrstuvwxyz";

    /**
     * 常用字符 大写字母
     */
    public final static String CHAR_LETTERS_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 常用字符 小写字母
     */
    public final static String CHAR_LETTERS_LOWER_AND_UPPER = CHAR_LETTERS_LOWER + CHAR_LETTERS_UPPER;

    /**
     * 常用字符 数字
     */
    public final static String CHAR_NUMBERS = "0123456789";

    /**
     * 常用字符 字母 + 数字
     */
    public final static String CHAR_LETTERS_AND_NUMBERS = CHAR_LETTERS_LOWER_AND_UPPER + CHAR_NUMBERS;

    /**
     * 常用字符 字母 + 特殊字符
     */
    public final static String CHAR_LETTERS_AND_SPECIAL_CHARACTERS = CHAR_LETTERS_LOWER_AND_UPPER + CHAR_SPECIAL_CHARACTERS;

    /**
     * 常用字符 字母 + 特殊字符 + 数字
     */
    public final static String CHAR_LETTERS_AND_SPECIAL_CHARACTERS_AND_NUMBERS = CHAR_LETTERS_AND_SPECIAL_CHARACTERS + CHAR_NUMBERS;

    /**
     * 常用字符 用户登录名（小字母 + 数字 + 下划线 + 减号）
     */
    public final static String CHAR_USER_LOGIN_NAME = CHAR_LETTERS_LOWER + CHAR_NUMBERS + "-_";

}
