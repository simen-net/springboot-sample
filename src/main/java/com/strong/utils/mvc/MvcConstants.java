package com.strong.utils.mvc;

/**
 * MVC模块的常量类
 *
 * @author Simen
 * @version 1.0 2018-06-29 17:25:26
 */
public class MvcConstants {

    /**
     * ID参数名
     */
    public final static String PARAMETER_INT_ID = "intId";

    /**
     * ID参数正在表达式 URL
     */
    public final static String PARAMETER_INT_ID_REGULAR = "/{" + PARAMETER_INT_ID + ":^\\d+$}";

    /**
     * ID数据组参数名
     */
    public final static String PARAMETER_INTS_ID = "intsId";

    /**
     * ID数据组参数正在表达式 URL
     */
    public final static String PARAMETER_INTS_ID_REGULAR = "/{" + PARAMETER_INTS_ID + ":^\\d+[,\\d+]*$}";

    /**
     * 用户信息URL
     */
    public final static String URL_USER_INFO = "userInfo";

    /**
     * 显示记录URL
     */
    public final static String URL_RECORD_VIEW = "record_view";

    /**
     * 用ID作为条件显示记录URL
     */
    public final static String URL_RECORD_VIEW_INT_ID = URL_RECORD_VIEW + PARAMETER_INT_ID_REGULAR;

    /**
     * 添加执行URL
     */
    public final static String URL_CREATE_ACTION = "create_action";

    /**
     * 添加显示URL
     */
    public final static String URL_CREATE_VIEW = "create_view";

    /**
     * 用ID作为条件显示添加界面记录URL
     */
    public final static String URL_CREATE_VIEW_INT_ID = URL_CREATE_VIEW + PARAMETER_INT_ID_REGULAR;

    /**
     * 查询完整记录显示URL
     */
    public final static String URL_LIST_VIEW = "list_view";

    /**
     * 查询分页记录显示URL
     */
    public final static String URL_PAGE_VIEW = "page_view";

    /**
     * 修改执行URL
     */
    public final static String URL_UPDATE_ACTION = "update_action";

    /**
     * 修改显示URL
     */
    public final static String URL_UPDATE_VIEW = "update_view";

    /**
     * 用ID作为条件显示修改界面记录URL
     */
    public final static String URL_UPDATE_VIEW_INT_ID = URL_UPDATE_VIEW + PARAMETER_INT_ID_REGULAR;

    /**
     * 删除执行URL
     */
    public final static String URL_DELETE_ACTION = "delete_action";

    /**
     * 用ID作为条件显示记录URL
     */
    public final static String URL_DELETE_ACTION_INTS_ID = URL_DELETE_ACTION + PARAMETER_INTS_ID_REGULAR;

    /**
     * 列表显示（无导航树）URL
     */
    public final static String URL_LIST_NO_TREE = "list";

    /**
     * 列表显示（有导航树）URL
     */
    public final static String URL_LIST_AND_TREE = "list_tree";

    /**
     * 文件上传URL
     */
    public final static String URL_FILE_UPLOAD = "file_upload";

    /**
     * 文件预览URL
     */
    public final static String URL_FILE_PREVIEW = "file_preview";

    /**
     * 图片上传URL
     */
    public final static String URL_IMAGE_UPLOAD = "image_upload";

    /**
     * 图片预览URL
     */
    public final static String URL_IMAGE_PREVIEW = "image_preview";
}
