package com.strong.utils.mvc.cache;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class CacheUtils {

    /**
     * 缓存管理器
     */
    @Autowired
    protected CacheManager cacheManager;

    public interface CallbackHandleCache {
        void handleNativeCache(Map<?, ?> nativeCache);
    }

    /**
     * 断言缓存存在
     *
     * @param strTableEntity 实体名称
     * @param strMethod 方法名称
     * @param intRecordCount 待比较记录计数
     */
    public void assertHasCache(String strTableEntity, String strMethod, Integer intRecordCount) {
        Cache cache = cacheManager.getCache(strTableEntity);
        assert cache != null;
        Map<?, ?> nativeCache = (Map<?, ?>) cache.getNativeCache();
        boolean hasCache = false;
        int intCacheCount = 0;
        for (Object objKey : nativeCache.keySet()) {
            assert objKey instanceof String;
            String strKey = objKey.toString();
            if (StrUtil.contains(strKey, strMethod)) {
                hasCache = true;

                Object objValue = nativeCache.get(objKey);
                if (objValue instanceof Page<?> pageValue) {
                    intCacheCount += pageValue.getContent().size();
                } else if (objValue instanceof List<?> listValue) {
                    intCacheCount = listValue.size();
                } else {
                    intCacheCount += 1;
                }
            }
        }
        Assert.isTrue(hasCache, "【{}】缓存不存在", strMethod);
        Assert.isTrue(intCacheCount == intRecordCount, "缓存数量与查询记录不一致");
    }

    /**
     * 断言缓存被强制清空
     *
     */
    public void assertsCacheEvicted() {
        // 遍历缓存内容
        for (String cacheName : cacheManager.getCacheNames()) {
            Cache cache = cacheManager.getCache(cacheName);
            assert cache != null;
            Map<?, ?> nativeCache = (Map<?, ?>) cache.getNativeCache();
            Assert.isTrue(ObjUtil.isNotNull(nativeCache) && MapUtil.isEmpty(nativeCache),
                    "缓存未清空：\n{}", nativeCache);
        }
    }


    /**
     * 遍历缓存内容
     *
     * @param cacheManager 缓存管理器
     */
    public void traverseCacheContents(CacheManager cacheManager) {
        traverseCacheContents(cacheManager, null);
    }


    /**
     * 遍历缓存内容
     *
     * @param cacheManager 缓存管理器
     */
    public void traverseCacheContents(CacheManager cacheManager, CallbackHandleCache callbackHandleCache) {
        // 遍历缓存内容s
        for (String cacheName : cacheManager.getCacheNames()) {
            Cache cache = cacheManager.getCache(cacheName);
            assert cache != null;
            Map<?, ?> nativeCache = (Map<?, ?>) cache.getNativeCache();
            if (ObjUtil.isNotNull(callbackHandleCache)) {
                callbackHandleCache.handleNativeCache(nativeCache);
            } else {
                for (Object objKey : nativeCache.keySet()) {
                    System.out.printf("缓存KEY：%s\n缓存记录：%s\n", objKey, nativeCache.get(objKey));
                }
            }
        }
    }
}
