package com.strong.utils.mvc.pojo.view;

import lombok.Getter;

/**
 * 用于与前端交互响应枚举
 *
 * @author Administrator
 * @date 2022/01/08
 */
@Getter
public enum ReplyEnum {

    /**
     * 服务器成功返回请求数据
     */
    SUCCESS_RETURN_DATA(200, "服务器成功返回请求数据"),

    /**
     * 服务器成功修改数据
     */
    SUCCESS_UPDATE_DATA(200, "修改操作成功"),

    /**
     * 服务器成功新建数据
     */
    SUCCESS_CREATE_DATA(200, "添加操作成功"),

    /**
     * 服务器成功新建数据
     */
    SUCCESS_DELETE_DATA(200, "删除操作成功"),

    /**
     * 新建或修改数据成功
     */
    SUCCESS_CREATE_OR_MODIFY_DATA(201, "新建或修改数据成功"),

    /**
     * 一个请求已经进入后台排队(异步任务)
     */
    SUCCESS_ENTER_BACKGROUND_QUEUE(202, "一个请求已经进入后台排队"),

    /**
     * 服务器成功处理，但未返回内容
     */
    SUCCESS_CONTENT_BLANK(204, "一个请求已经进入后台排队"),

    /**
     * 发出信息有误
     */
    ERROR_SEND_WRONG_MESSAGE(400, "发出信息有误"),

    /**
     * 用户没有权限(令牌失效、用户名、密码错误、登录过期)
     */
    ERROR_USER_HAS_NO_PERMISSIONS(401, "用户没有权限"),

    /**
     * 用户不存在
     */
    ERROR_USER_NOT_EXIST(401, "用户不存在"),

    /**
     * 令牌过期
     */
    ERROR_TOKEN_EXPIRED(402, "令牌过期、未授权或登录已超时"),

    /**
     * 用户得到授权，但是该访问是被禁止的
     */
    ERROR_ACCESS_IS_FORBIDDEN(403, "用户得到授权，但是该访问是被禁止的"),

    /**
     * 访问资源不存在
     */
    ERROR_TRESOURCE_NOT_EXIST(404, "访问资源不存在"),

    /**
     * 请求格式不可得
     */
    ERROR_FORMAT_NOT_AVAILABLE(406, "请求格式不可得"),

    /**
     * 请求资源被永久删除，且不会被看到
     */
    ERROR_TRESOURCE_IS_ELETED(410, "请求资源被永久删除，且不会被看到"),

    /**
     * 服务器发生错误
     */
    ERROR_SERVER_ERROR(500, "服务器返回错误"),

    /**
     * 网关错误
     */
    ERROR_GATEWAY_ERROR(502, "网关错误"),

    /**
     * 服务不可用，服务器暂时过载或维护
     */
    ERROR_SERVICE_UNAVAILABLE(503, "服务不可用，服务器暂时过载或维护"),

    /**
     * 网关超时
     */
    ERROR_GATEWAY_TIMEOUT(504, "网关超时"),


    // ========================= 常用500错误 =============

    /**
     * 服务器发生错误 - 提交的参数异常
     */
    ERROR_SERVER_ILLEGAL_ARGUMENT_EXCEPTION(500, "提交的参数异常"),

    /**
     * 服务器发生错误 - 提交的数据无效
     */
    ERROR_SERVER_METHOD_ARGUMENT_NOT_VALID_EXCEPTION(500, "提交的数据无效"),

    /**
     * 服务器发生错误 - 数据库实体异常
     */
    ERROR_SERVER_ENTITY_NOT_FOUND_EXCEPTION(500, "数据库实体异常"),

    /**
     * 服务器发生错误 - 未授权或登录已超时
     */
    ERROR_SERVER_ACCESS_DENIED_EXCEPTION(500, "未授权或登录已超时"),

    /**
     * 服务器发生错误 - 数据库处理异常
     */
    ERROR_SERVER_SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION(500, "数据库处理异常"),

    /**
     * 服务器发生错误 - 非法状态异常
     */
    ERROR_SERVER_ILLEGAL_STATE_EXCEPTION(500, "非法状态异常"),

    /**
     * 服务器发生错误 - 数据完整性检查异常
     */
    ERROR_SERVER_DATA_INTEGRITY_VIOLATION_EXCEPTION(500, "数据完整性检查异常"),

    /**
     * 服务器发生错误 - 用户验证失效
     */
    ERROR_SERVER_BAD_CREDENTIALS_EXCEPTION(500, "用户验证失效"),

    /**
     * 上传文件写入失败
     */
    ERROR_SERVER_WRITE_UPLOADED_FILE_FAILED(500, "上传文件写入失败");

    /**
     * 返回代码
     */
    private Integer code;

    /**
     * 返回消息
     */
    private String msg;

    /**
     * 实例化方法
     *
     * @param code 返回代码
     * @param msg  返回消息
     */
    ReplyEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
