package com.strong.utils.mvc.pojo.view.vab;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * vab列表模型
 *
 * @author simen
 * @date 2022/02/23
 */
@Data
@NoArgsConstructor
public class VabList<M> {

    public VabList(List<M> list, Long total) {
        this.list = list;
        this.total = total;
    }

    /**
     * 返回数组
     */
    private List<M> list;

    /**
     * 总条数
     */
    private Long total;
}
