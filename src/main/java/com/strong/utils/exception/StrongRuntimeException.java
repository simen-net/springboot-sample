package com.strong.utils.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class StrongRuntimeException extends RuntimeException {

    /**
     * 错误map
     */
    private Map<String, String> mapError;

    public StrongRuntimeException(String message) {
        super(message);
    }

    public StrongRuntimeException(Map<String, String> mapError) {
        this.mapError = mapError;
    }
}
