package com.strong.utils.message;

import cn.hutool.core.util.StrUtil;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * 自定义消息实现类
 *
 * @author Simen
 * @version 1.0 2018-06-12 20:41:46
 */
@Component
public class StrongMessageImpl implements StrongMessageSource {

    /**
     * 注入Spring消息类
     */
    protected final MessageSource messageSource;

    /**
     * 默认消息代码
     */
    public final static String DEFAULT_MESSAGE = "DEFAULT_MESSAGE";

    /**
     * 默认错误代码
     */
    public final static String DEFAULT_ERROR = "DEFAULT_ERROR";

    /**
     * 实例化
     *
     * @param messageSource 消息来源
     */
    public StrongMessageImpl(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * 根据代码和参数代码返回消息文本
     *
     * @param strCode       消息代码
     * @param strsParameter 消息参数
     * @return the message
     */
    @Override
    public String getMessage(String strCode, String... strsParameter) {
        return getMessage(strCode, (Object[]) strsParameter);
    }

    /**
     * 根据代码和参数代码返回消息文本
     *
     * @param strCode       消息代码
     * @param objsParameter 消息参数对象
     * @return the message
     */
    @Override
    public String getMessage(String strCode, Object... objsParameter) {
        if (StrUtil.isBlank(strCode)) {
            return messageSource.getMessage(DEFAULT_MESSAGE, objsParameter, Locale.getDefault());
        } else {
            return messageSource.getMessage(strCode, objsParameter, DEFAULT_MESSAGE, Locale.getDefault());
        }
    }

    /**
     * 根据代码和参数代码返回错误消息文本，如果没有文本则返回"未知错误"
     *
     * @param strCode       消息代码
     * @param strsParameter 消息参数
     * @return the error
     */
    @Override
    public String getError(String strCode, String... strsParameter) {
        return getError(strCode, (Object[]) strsParameter);
    }

    /**
     * 根据代码和参数代码返回错误消息文本，如果没有文本则返回"未知错误"
     *
     * @param strCode       消息代码
     * @param objsParameter 消息参数对象
     * @return the error
     */
    @Override
    public String getError(String strCode, Object... objsParameter) {
        if (StrUtil.isBlank(strCode)) {
            return messageSource.getMessage(DEFAULT_ERROR, objsParameter, Locale.getDefault());
        } else {
            return messageSource.getMessage(strCode, objsParameter, DEFAULT_ERROR, Locale.getDefault());
        }
    }
}
