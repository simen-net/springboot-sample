package com.strong.utils.message;

/**
 * 自定义消息处理接口.
 *
 * @author Simen
 * @version 1.0
 * @date 2022/01/11
 */
public interface StrongMessageSource {

    /**
     * 根据代码和参数代码返回消息文本
     *
     * @param strCode       消息代码
     * @param strsParameter 消息参数
     * @return the message
     */
    String getMessage(String strCode, String... strsParameter);

    /**
     * 根据代码和参数代码返回消息文本
     *
     * @param strCode       消息代码
     * @param objsParameter 消息参数对象
     * @return the message
     */
    String getMessage(String strCode, Object... objsParameter);

    /**
     * 根据代码和参数代码返回错误消息文本，如果没有文本则返回"未知错误"
     *
     * @param strCode       消息代码
     * @param strsParameter 消息参数
     * @return the error
     */
    String getError(String strCode, String... strsParameter);

    /**
     * 根据代码和参数代码返回错误消息文本，如果没有文本则返回"未知错误"
     *
     * @param strCode       消息代码
     * @param objsParameter 消息参数对象
     * @return the error
     */
    String getError(String strCode, Object... objsParameter);
}
