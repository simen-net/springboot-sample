package com.strong.utils.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * jwt响应数据
 *
 * @author simen
 * @date 2022/02/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponseData {

    /**
     * 令牌
     */
    private String token;

    /**
     * 获取令牌的时间
     */
    private Date date;
}
