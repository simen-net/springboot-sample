package com.strong.utils.jpa;//package com.strong.utils.jpa;
//
//import com.alibaba.fastjson2.JSON;
//import com.strong.utils.DateFormatUtils;
//import com.strong.utils.RandomUtils;
//import com.strong.utils.StringUtils;
//import org.hibernate.annotations.Type;
//
//import javax.persistence.Column;
//import java.beans.IntrospectionException;
//import java.beans.PropertyDescriptor;
//import java.lang.reflect.Field;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 实体工具类.
// *
// * @author Simen
// * @version 1.0
// */
//public class EntityUtils extends org.apache.commons.lang3.RandomUtils {
//
//    /**
//     * 获取随机属性的bean对应的map集合，默认日期格式
//     *
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    public static <T> Map<String, String> getMap(Class<?> clazz) {
//        return getMap(clazz, DateFormatUtils.ISO_8601_SIMPLE_FORMAT.getPattern());
//    }
//
//    /**
//     * 获取随机属性的bean对应的map集合，指定日期格式
//     *
//     * @param clazz
//     * @param dateParse 日期格式化成字符串的解析
//     * @param <T>
//     * @return
//     */
//    public static <T> Map<String, String> getMap(Class<?> clazz, String dateParse) {
//        T t = getObject(clazz);
//        if (t == null) {
//            return null;
//        }
//        Field[] fields = clazz.getDeclaredFields();
//        //属性字段
//        String fileName = null;
//        String fileValue = null;
//        Map<String, String> mapField = new HashMap<>(16);
//        for (int i = 0; i < fields.length; i++) {
//            //属性字段
//            fileName = fields[i].getName();
//            //属性值
//            try {
//                boolean accessFlag = fields[i].isAccessible();
//                // 修改访问控制权限
//                fields[i].setAccessible(true);
//
//                Object objValue = fields[i].get(t);
//                if (objValue != null) {
//                    if (objValue instanceof Date.class)) {
//                        fileValue = DateFormatUtils.getString((Date) objValue, dateParse);
//                    } else {
//                        fileValue = objValue.toString();
//                    }
//                }
//
//                // 恢复访问控制权限
//                fields[i].setAccessible(accessFlag);
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            }
//            if (!StringUtils.isAnyBlank(fileName, fileValue)) {
//                mapField.put(fileName, fileValue);
//            }
//        }
//        return mapField;
//    }
//
//    /**
//     * 获取随机属性的bean
//     *
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    public static <T> T getObject(Class<?> clazz) {
//        T t = null;
//        if (clazz == null) {
//            return t;
//        }
//        //需要有无参的构造器
//        try {
//            t = (T) clazz.getDeclaredConstructor().newInstance();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        Field[] fields = clazz.getDeclaredFields();
//        //属性字段
//        String fileName = "";
//        //符合JavaBean规则的属性
//        PropertyDescriptor property = null;
//        //set方法对象
//        Method method = null;
//        for (int i = 0; i < fields.length; i++) {
//            //属性字段
//            fileName = fields[i].getName();
//            //获取属性上的注解
//            Column annotationColumn = fields[i].getAnnotation(Column.class);
//            Type annotationType = fields[i].getAnnotation(Type.class);
//            try {
//                property = new PropertyDescriptor(fileName, clazz);
//            } catch (IntrospectionException e) {
//                //没有设置set方法，或者不符合JavaBean规范
//                continue;
//            }
//            //获取set方法对象
//            method = property.getWriteMethod();
//            //如果是字节类型(包含基本类型和包装类)
//            if (fields[i].getType() == byte.class || fields[i].getType() == Byte.class) {
//                try {
//                    method.invoke(t, nextBytes(1)[0]);
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是short类型(包含基本类型和包装类)
//            else if (fields[i].getType() == short.class || fields[i].getType() == Short.class) {
//                try {
//                    method.invoke(t, (short) nextInt());
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是char类型(包含基本类型和包装类)
//            else if (fields[i].getType() == char.class || fields[i].getType() == Character.class) {
//                try {
//                    method.invoke(t, Character.valueOf((char) nextInt()));
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是整型(包含基本类型和包装类)
//            else if (fields[i].getType() == int.class || fields[i].getType() == Integer.class) {
//                try {
//                    //为属性赋值
//                    method.invoke(t, nextInt());
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是float(包含基本类型和包装类)
//            else if (fields[i].getType() == float.class || fields[i].getType() == Float.class) {
//                try {
//                    //为属性赋值
//                    method.invoke(t, nextFloat());
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是double(包含基本类型和包装类)
//            else if (fields[i].getType() == double.class || fields[i].getType() == Double.class) {
//                try {
//                    //为属性赋值
//                    method.invoke(t, nextDouble());
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是double(包含基本类型和包装类)
//            else if (fields[i].getType() == long.class || fields[i].getType() == Long.class) {
//                try {
//                    //为属性赋值
//                    method.invoke(t, nextLong());
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是boolean(包含基本类型和包装类)
//            else if (fields[i].getType() == boolean.class || fields[i].getType() == Boolean.class) {
//                try {
//                    //为属性赋值
//                    method.invoke(t, nextBoolean());
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是boolean(包含基本类型和包装类)
//            else if (fields[i].getType() == String.class) {
//                try {
//                    int intLength = 10;
//                    String strType = null;
//
//                    // 根据注解获取字符串长度限制
//                    if (annotationColumn != null) {
//                        intLength = annotationColumn.length();
//                    }
//
//                    // 根据注解获取字段特殊类型
//                    if (annotationType != null) {
//                        strType = annotationType.type();
//                    }
//
//                    // 限制长度在100以内，且大于100生成随机中文，否则随机字符+数字
//                    String str = intLength > 100 ? RandomUtils.getChineseString(100) : RandomUtils.getLetterAndNumber(intLength);
//                    str = str.replace("\00", "").replace('\0', ' ').split("\u0000")[0];
//
//                    // 判断是否为特殊类型
//                    if (strType == null) {
//                        // 为属性赋值
//                        method.invoke(t, str);
//                    } else if (strType.equals("JsonbType")) {
//                        Map<String, String> _map = new HashMap<>();
//                        for (int j = 0; j < RandomUtils.getInt(10, 50, true); j++) {
//                            _map.put(RandomUtils.getLetterAndNumber(10), RandomUtils.getChineseString(100));
//                        }
//                        method.invoke(t, JSON.toJSONString(_map));
//                    }
////                    method.invoke(t, "错误: 无效的 \"UTF8\" 编码字节顺序: 0x00");
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            //如果是日期类型
//            else if (fields[i].getType() == Date.class) {
//                try {
//                    method.invoke(t, RandomUtils.getDate());
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return t;
//    }
//}
