package com.strong.sample.table.t_test.model;

import com.strong.sample.table.t_test.TableTestConstants;
import com.strong.sample.table.t_test.jpa.TableTestDO;
import com.strong.utils.CodeUtils;
import com.strong.utils.StrongUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 测试表 显示模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TableTestVO extends TableTestAO {

    /**
     * 序列化包含的字段数组
     */
    public final static String[] STRS_INCLUDE_PROPERTIES = {
            //  字段实体名
            TableTestConstants.TEST_ID_ENTITY,
            // 字符串字段 字段实体名
            TableTestConstants.TEST_STR_ENTITY,
            // 整数字段 字段实体名
            TableTestConstants.TEST_INT_ENTITY,
            // 日期字段 字段实体名
            TableTestConstants.TEST_DATE_ENTITY,
            // 日期时间字段 字段实体名
            TableTestConstants.TEST_DATETIME_ENTITY,
            // 浮点字段 字段实体名
            TableTestConstants.TEST_FLOAT_ENTITY,
            // 布尔字段 字段实体名
            TableTestConstants.TEST_BOOL_ENTITY,
//            // 长文本字段 字段实体名
//            TableTestConstants.TEST_TEXT_ENTITY,
    };

    /**
     * 自动生成序列化不包含的字段数组
     */
    public final static String[] STRS_EXCLUSION_PROPERTIES = StrongUtils.getExclusionFieldNames(TableTestVO.class);

    /**
     * 以实体类实例化
     *
     * @param tableTestDO 实体类
     */
    public TableTestVO(final TableTestDO tableTestDO) {
        this(tableTestDO, STRS_INCLUDE_PROPERTIES);
    }

    /**
     * 以实体类实例化 仅包含strsIncludeProperties的属性
     *
     * @param tableTestDO           实体类
     * @param strsIncludeProperties 包含的字段数组
     */
    public TableTestVO(final TableTestDO tableTestDO, final String[] strsIncludeProperties) {
        StrongUtils.copyProperties(tableTestDO, this, strsIncludeProperties);
    }

    /**
     * 主入口
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        String strClassName = Thread.currentThread().getStackTrace()[1].getClassName();
        CodeUtils.printTsModelCode(strClassName, STRS_INCLUDE_PROPERTIES);
    }

    // ===================扩展=======================

}
