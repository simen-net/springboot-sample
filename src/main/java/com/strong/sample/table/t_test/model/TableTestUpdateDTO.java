package com.strong.sample.table.t_test.model;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.strong.utils.annotation.StrongRemark;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Date;

/**
 * 测试表 修改模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
public class TableTestUpdateDTO {

    /**
     * <pre>
     *     @Null 被注释的元素必须为null
     *     @NotNull 被注释的元素不能为null
     *     @AssertTrue 被注释的元素必须为true
     *     @AssertFalse 被注释的元素必须为false
     *     @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @Size(max,min) 被注释的元素的大小必须在指定的范围内。
     *     @Digits(integer,fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
     *     @Past 被注释的元素必须是一个过去的日期
     *     @Future 被注释的元素必须是一个将来的日期
     *     @Pattern(value) 被注释的元素必须符合指定的正则表达式。
     *     @Email 被注释的元素必须是电子邮件地址
     *     @Length 被注释的字符串的大小必须在指定的范围内
     *     @NotEmpty 被注释的字符串必须非空
     *     @Range 被注释的元素必须在合适的范围内
     * </pre>
     */

    @NotNull
    @StrongRemark("")
    private Integer testId;

    @Size(message = "字符串字段长度必须小于32个字符", max = 32)
    @StrongRemark("字符串字段")
    private String testStr;

    @StrongRemark("整数字段")
    private Integer testInt;

    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @StrongRemark("日期字段")
    private Date testDate;

    @StrongRemark("日期时间字段")
    private Date testDatetime;

    @StrongRemark("浮点字段")
    private Float testFloat;

    @StrongRemark("布尔字段")
    private String testBool;

    @Size(message = "长文本字段长度必须小于1,000,000,000个字符", max = 1000000000)
    @StrongRemark("长文本字段")
    private String testText;


    // ===================扩展=======================

}