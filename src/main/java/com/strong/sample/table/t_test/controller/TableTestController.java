package com.strong.sample.table.t_test.controller;

import com.strong.sample.table.t_test.TableTestConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 测试表 HTML控制器
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
@Slf4j
@Controller
@RequestMapping(value = TableTestConstants.TABLE_ENTITY)
public class TableTestController {

}
