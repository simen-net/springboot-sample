package com.strong.sample.table.t_test.jpa;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;

import java.util.Date;

/**
 * 测试表 托管类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@StaticMetamodel(TableTestDO.class)
public class TableTestDO_ {
    /**
     *
     */
    public static volatile SingularAttribute<TableTestDO, Integer> testId;

    /**
     * 字符串字段
     */
    public static volatile SingularAttribute<TableTestDO, String> testStr;

    /**
     * 整数字段
     */
    public static volatile SingularAttribute<TableTestDO, Integer> testInt;

    /**
     * 日期字段
     */
    public static volatile SingularAttribute<TableTestDO, Date> testDate;

    /**
     * 日期时间字段
     */
    public static volatile SingularAttribute<TableTestDO, Date> testDatetime;

    /**
     * 浮点字段
     */
    public static volatile SingularAttribute<TableTestDO, String> testFloat;

    /**
     * 布尔字段
     */
    public static volatile SingularAttribute<TableTestDO, String> testBool;

    /**
     * 长文本字段
     */
    public static volatile SingularAttribute<TableTestDO, String> testText;

}