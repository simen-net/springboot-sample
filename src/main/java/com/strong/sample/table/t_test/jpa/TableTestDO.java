package com.strong.sample.table.t_test.jpa;

import com.strong.sample.table.t_test.TableTestConstants;
import com.strong.utils.annotation.StrongRemark;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;

/**
 * 测试表 DO实体类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = TableTestConstants.TABLE_NAME)
public class TableTestDO implements java.io.Serializable {

    @Id
    @Column(name = TableTestConstants.TEST_ID_COLUMN, unique = true, nullable = false)
    @StrongRemark("主键")
    private Integer testId;

    @Column(name = TableTestConstants.TEST_STR_COLUMN, length = 32)
    @StrongRemark("字符串字段")
    private String testStr;

    @Column(name = TableTestConstants.TEST_INT_COLUMN)
    @StrongRemark("整数字段")
    private Integer testInt;

    @Temporal(TemporalType.DATE)
    @Column(name = TableTestConstants.TEST_DATE_COLUMN)
    @StrongRemark("日期字段")
    private Date testDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = TableTestConstants.TEST_DATETIME_COLUMN)
    @StrongRemark("日期时间字段")
    private Date testDatetime;

    @Column(name = TableTestConstants.TEST_FLOAT_COLUMN)
    @StrongRemark("浮点字段")
    private Float testFloat;

    @Column(name = TableTestConstants.TEST_BOOL_COLUMN)
    @StrongRemark("布尔字段")
    private String testBool;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = TableTestConstants.TEST_TEXT_COLUMN, length = 1000000000)
    @StrongRemark("长文本字段")
    private String testText;

    // ===================扩展=======================
}

