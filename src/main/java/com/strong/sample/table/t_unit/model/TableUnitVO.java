package com.strong.sample.table.t_unit.model;

import com.strong.sample.table.t_unit.TableUnitConstants;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;
import com.strong.utils.CodeUtils;
import com.strong.utils.StrongUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 单位表 显示模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TableUnitVO extends TableUnitAO {

    /**
     * 序列化包含的字段数组
     */
    public final static String[] STRS_INCLUDE_PROPERTIES = {
        // 系统随机生成固定编号 字段实体名
        TableUnitConstants.UNIT_ID_ENTITY,
        // 单位名称 字段实体名
        TableUnitConstants.UNIT_NAME_ENTITY,
        // 单位显示编码 字段实体名
        TableUnitConstants.UNIT_CODE_ENTITY,
        // 单位类型编码 字段实体名
        TableUnitConstants.UNIT_TYPE_ENTITY,
        // 单位名称关键字 字段实体名
        TableUnitConstants.UNIT_NAME_KEYWORD_ENTITY,
    };

    /**
     * 自动生成序列化不包含的字段数组
     */
    public final static String[] STRS_EXCLUSION_PROPERTIES = StrongUtils.getExclusionFieldNames(TableUnitVO.class);

    /**
     * 以实体类实例化
     *
     * @param tableUnitDO 实体类
     */
    public TableUnitVO(final TableUnitDO tableUnitDO) {
        this(tableUnitDO, STRS_INCLUDE_PROPERTIES);
    }

    /**
     * 以实体类实例化 仅包含strsIncludeProperties的属性
     *
     * @param tableUnitDO           实体类
     * @param strsIncludeProperties 包含的字段数组
     */
    public TableUnitVO(final TableUnitDO tableUnitDO, final String[] strsIncludeProperties) {
        StrongUtils.copyProperties(tableUnitDO, this, strsIncludeProperties);
    }

    /**
     * 主入口
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        String strClassName = Thread.currentThread().getStackTrace()[1].getClassName();
        CodeUtils.printTsModelCode(strClassName, STRS_INCLUDE_PROPERTIES);
    }

    // ===================扩展=======================

}
