package com.strong.sample.table.t_unit.model;

import lombok.*;
import cn.hutool.core.date.DatePattern;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import com.strong.utils.mvc.pojo.retrieve.VabDTO;

/**
 * 单位表 查询模型类
 *
 * @author Simen
 * @date 2024-11-19 17:20:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TableUnitRetrieveDTO extends VabDTO<TableUnitVO> {

}