package com.strong.sample.table.t_unit;

/**
 * 单位表 常量类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public class TableUnitConstants {

    /**
     * 单位表 数据库表备注
     */
    public final static String TABLE_COMMENT = "单位表";

    /**
     * 单位表 数据库表名
     */
    public final static String TABLE_NAME = "T_UNIT";

    /**
     * 单位表 表实体名
     */
    public final static String TABLE_ENTITY = "tableUnit";

    /**
     * 单位表 表实体类模块名
     */
    public final static String TABLE_PACKAGE = "t_unit";

    /**
     * 系统随机生成固定编号 字段备注
     */
    public final static String UNIT_ID_COMMENT = "系统随机生成固定编号";

    /**
     * 系统随机生成固定编号 数据库字段名
     */
    public final static String UNIT_ID_COLUMN = "UNIT_ID";

    /**
     * 系统随机生成固定编号 字段实体名
     */
    public final static String UNIT_ID_ENTITY = "unitId";
    /**
     * 单位名称 字段备注
     */
    public final static String UNIT_NAME_COMMENT = "单位名称";

    /**
     * 单位名称 数据库字段名
     */
    public final static String UNIT_NAME_COLUMN = "UNIT_NAME";

    /**
     * 单位名称 字段实体名
     */
    public final static String UNIT_NAME_ENTITY = "unitName";
    /**
     * 单位显示编码 字段备注
     */
    public final static String UNIT_CODE_COMMENT = "单位显示编码";

    /**
     * 单位显示编码 数据库字段名
     */
    public final static String UNIT_CODE_COLUMN = "UNIT_CODE";

    /**
     * 单位显示编码 字段实体名
     */
    public final static String UNIT_CODE_ENTITY = "unitCode";
    /**
     * 单位类型编码 字段备注
     */
    public final static String UNIT_TYPE_COMMENT = "单位类型编码";

    /**
     * 单位类型编码 数据库字段名
     */
    public final static String UNIT_TYPE_COLUMN = "UNIT_TYPE";

    /**
     * 单位类型编码 字段实体名
     */
    public final static String UNIT_TYPE_ENTITY = "unitType";
    /**
     * 单位名称关键字 字段备注
     */
    public final static String UNIT_NAME_KEYWORD_COMMENT = "单位名称关键字";

    /**
     * 单位名称关键字 数据库字段名
     */
    public final static String UNIT_NAME_KEYWORD_COLUMN = "UNIT_NAME_KEYWORD";

    /**
     * 单位名称关键字 字段实体名
     */
    public final static String UNIT_NAME_KEYWORD_ENTITY = "unitNameKeyword";

    // ===================扩展=======================
}
