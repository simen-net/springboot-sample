package com.strong.sample.table.t_unit.jpa;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.util.Date;
import java.math.BigDecimal;

/**
* 单位表 托管类
*
* @author Simen
* @date 2024-11-19 17:20:57
*/
@StaticMetamodel(TableUnitDO.class)
public class TableUnitDO_ {
    /**
    * 系统随机生成固定编号
    */
    public static volatile SingularAttribute<TableUnitDO, Integer> unitId;

    /**
    * 单位名称
    */
    public static volatile SingularAttribute<TableUnitDO, String> unitName;

    /**
    * 单位显示编码
    */
    public static volatile SingularAttribute<TableUnitDO, String> unitCode;

    /**
    * 单位类型编码
    */
    public static volatile SingularAttribute<TableUnitDO, String> unitType;

    /**
    * 单位名称关键字
    */
    public static volatile SingularAttribute<TableUnitDO, String> unitNameKeyword;

}