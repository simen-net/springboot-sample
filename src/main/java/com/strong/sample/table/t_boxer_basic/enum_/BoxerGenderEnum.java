package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 性别 枚举
 *
 * @author simen
 * @date 2023/02/16
 */
@Getter
public enum BoxerGenderEnum {

    MALE("M", "男"),
    FEMALE("F", "女");

    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回消息
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回值
     */
    BoxerGenderEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
