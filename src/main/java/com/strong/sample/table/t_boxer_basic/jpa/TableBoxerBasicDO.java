package com.strong.sample.table.t_boxer_basic.jpa;

import com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants;
import com.strong.utils.annotation.StrongRemark;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;
import java.util.Objects;

/**
 * 运动员基本信息表 DO实体类
 *
 * @author Simen
 * @date 2024-11-20 11:05:53
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = TableBoxerBasicConstants.TABLE_NAME)
public class TableBoxerBasicDO implements java.io.Serializable {

    @Id
    @Column(name = TableBoxerBasicConstants.BOXER_ID_COLUMN, unique = true, nullable = false)
    @StrongRemark("系统随机生成固定编号")
    private Integer boxerId;

    @Column(name = TableBoxerBasicConstants.BOXER_ATHLETE_ID_COLUMN, length = 64, unique = true, nullable = false)
    @StrongRemark("运动员编号（不重复）")
    private String boxerAthleteId;

    @Column(name = TableBoxerBasicConstants.BOXER_NAME_COLUMN, length = 32, nullable = false)
    @StrongRemark("姓名")
    private String boxerName;

    @Column(name = TableBoxerBasicConstants.BOXER_PHONETIC_INITIALS_COLUMN, length = 32)
    @StrongRemark("拼音首字母")
    private String boxerPhoneticInitials;

    @Column(name = TableBoxerBasicConstants.BOXER_PHONETIC_SURNAME_COLUMN, length = 32)
    @StrongRemark("姓的拼音")
    private String boxerPhoneticSurname;

    @Column(name = TableBoxerBasicConstants.BOXER_ENGLISH_NAME_COLUMN, length = 64)
    @StrongRemark("英文名")
    private String boxerEnglishName;

    @Column(name = TableBoxerBasicConstants.BOXER_LOCAL_REGISTER_INFO_ID_COLUMN, length = 64, unique = true)
    @StrongRemark("[外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键")
    private String boxerLocalRegisterInfoId;

    @Column(name = TableBoxerBasicConstants.BOXER_UNIT_ID_COLUMN)
    @StrongRemark("[外键]运动员所在单位ID - 关联T_UNIT主键")
    private Integer boxerUnitId;

    @Column(name = TableBoxerBasicConstants.BOXER_ID_NUMBER_COLUMN, length = 30, nullable = false)
    @StrongRemark("身份证号码")
    private String boxerIdNumber;

    @Column(name = TableBoxerBasicConstants.BOXER_GENDER_COLUMN, length = 8, nullable = false)
    @StrongRemark("性别编码")
    private String boxerGender;

    @Column(name = TableBoxerBasicConstants.BOXER_GENDER_NAME_COLUMN, length = 8)
    @StrongRemark("性别")
    private String boxerGenderName;

    @Column(name = TableBoxerBasicConstants.BOXER_NATIONALITY_COLUMN, length = 100)
    @StrongRemark("民族编码")
    private String boxerNationality;

    @Column(name = TableBoxerBasicConstants.BOXER_NATIONALITY_NAME_COLUMN, length = 100)
    @StrongRemark("民族")
    private String boxerNationalityName;

    @Column(name = TableBoxerBasicConstants.BOXER_NATIVE_PLACE_COLUMN, length = 100)
    @StrongRemark("户籍所在地")
    private String boxerNativePlace;

    @Temporal(TemporalType.DATE)
    @Column(name = TableBoxerBasicConstants.BOXER_BIRTHDAY_COLUMN, nullable = false)
    @StrongRemark("出生日期")
    private Date boxerBirthday;

    @Column(name = TableBoxerBasicConstants.BOXER_EDUCATION_COLUMN, length = 16)
    @StrongRemark("文化程度编码")
    private String boxerEducation;

    @Column(name = TableBoxerBasicConstants.BOXER_EDUCATION_NAME_COLUMN, length = 100)
    @StrongRemark("文化程度")
    private String boxerEducationName;

    @Column(name = TableBoxerBasicConstants.BOXER_GUARDIAN_ID_NUMBER_COLUMN, length = 30)
    @StrongRemark("监护人身份证号码")
    private String boxerGuardianIdNumber;

    @Column(name = TableBoxerBasicConstants.BOXER_GUARDIAN_MOBILE_COLUMN, length = 20)
    @StrongRemark("监护人手机号码")
    private String boxerGuardianMobile;

    @Column(name = TableBoxerBasicConstants.BOXER_GUARDIAN_NAME_COLUMN, length = 100)
    @StrongRemark("监护人姓名")
    private String boxerGuardianName;

    @Column(name = TableBoxerBasicConstants.BOXER_GUARDIAN_RELATION_COLUMN, length = 100)
    @StrongRemark("与被监护人关系")
    private String boxerGuardianRelation;

    @Column(name = TableBoxerBasicConstants.BOXER_HEALTH_COLUMN, length = 100)
    @StrongRemark("健康状态代码")
    private String boxerHealth;

    @Column(name = TableBoxerBasicConstants.BOXER_HEALTH_CONTENT_COLUMN, length = 100)
    @StrongRemark("健康状态内容")
    private String boxerHealthContent;

    @Column(name = TableBoxerBasicConstants.BOXER_HEALTH_NAME_COLUMN, length = 100)
    @StrongRemark("健康状态")
    private String boxerHealthName;

    @Column(name = TableBoxerBasicConstants.BOXER_STATUS_COLUMN, length = 100)
    @StrongRemark("运动员状态编码")
    private String boxerStatus;

    @Column(name = TableBoxerBasicConstants.BOXER_STATUS_NAME_COLUMN, length = 100)
    @StrongRemark("运动员状态")
    private String boxerStatusName;

    @Column(name = TableBoxerBasicConstants.BOXER_GRADE_COLUMN, length = 100)
    @StrongRemark("运动员级别编码")
    private String boxerGrade;

    @Column(name = TableBoxerBasicConstants.BOXER_GRADE_NAME_COLUMN, length = 100)
    @StrongRemark("运动员级别")
    private String boxerGradeName;

    @Column(name = TableBoxerBasicConstants.BOXER_TYPE_COLUMN, length = 20)
    @StrongRemark("")
    private String boxerType;

    @Column(name = TableBoxerBasicConstants.BOXER_EMAIL_COLUMN, length = 200)
    @StrongRemark("电子邮箱")
    private String boxerEmail;

    @Column(name = TableBoxerBasicConstants.BOXER_MOBILE_COLUMN, length = 20)
    @StrongRemark("运动员手机号码")
    private String boxerMobile;

    @Column(name = TableBoxerBasicConstants.BOXER_HAS_PHOTO_COLUMN)
    @StrongRemark("是否有照片")
    private Boolean boxerHasPhoto;

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Hibernate.getClass(this) != Hibernate.getClass(obj)) {
            return false;
        }
        TableBoxerBasicDO that = (TableBoxerBasicDO) obj;
        return boxerId != null && Objects.equals(boxerId, that.boxerId);
    }

    // ===================扩展=======================
}

