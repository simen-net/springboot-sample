package com.strong.sample.table.t_user.model;

import com.strong.sample.table.t_user.TableUserConstants;
import com.strong.sample.table.t_user.jpa.TableUserDO;
import com.strong.utils.CodeUtils;
import com.strong.utils.StrongUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 显示模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TableUserVO extends TableUserAO {

    /**
     * 序列化包含的字段数组
     */
    public final static String[] STRS_INCLUDE_PROPERTIES = {
            // 系统随机生成固定编号 字段实体名
            TableUserConstants.USER_ID_ENTITY,
            // 用户名 字段实体名
            TableUserConstants.USER_NAME_ENTITY,
            // 用户登录名 字段实体名
            TableUserConstants.USER_LOGIN_NAME_ENTITY,

//            // 用户登录密码 字段实体名
//            TableUserConstants.USER_LOGIN_PASSWORD_ENTITY,
//            // 用户登录密码加盐 字段实体名
//            TableUserConstants.USER_LOGIN_SALT_ENTITY,
//            // 用户权限 字段实体名
//            TableUserConstants.USER_AUTHORITY_ENTITY,
//            // 用户分组 字段实体名
//            TableUserConstants.USER_GROUP_ENTITY,
//            // [外键]对应运动队编号-关联T_TEAM外键 字段实体名
//            TableUserConstants.USER_TEAM_ORG_ID_ENTITY,
//            // [外键]用户所属单位-关联单位表主键 字段实体名
//            TableUserConstants.USER_UNIT_ID_ENTITY,
//            // 证件类型 字段实体名
//            TableUserConstants.USER_ID_TYPE_ENTITY,
//            // 证件号码 字段实体名
//            TableUserConstants.USER_ID_NUMBER_ENTITY,
//            // 用户手机号码 字段实体名
//            TableUserConstants.USER_MOBILE_PHONE_ENTITY,
//            // 用户固定电话 字段实体名
//            TableUserConstants.USER_TELEPHONE_ENTITY,
//            // 用户邮箱地址 字段实体名
//            TableUserConstants.USER_EMAIL_ENTITY,
//            // 用户QQ号码 字段实体名
//            TableUserConstants.USER_QQ_CODE_ENTITY,
//            // 用户微信号码 字段实体名
//            TableUserConstants.USER_WX_CODE_ENTITY,
//            // 用户登录记录 字段实体名
//            TableUserConstants.USER_LOGIN_RECORD_ENTITY,
    };

    /**
     * 自动生成序列化不包含的字段数组
     */
    public final static String[] STRS_EXCLUSION_PROPERTIES = StrongUtils.getExclusionFieldNames(TableUserVO.class);

    /**
     * 以实体类实例化
     *
     * @param tableUserDO 实体类
     */
    public TableUserVO(final TableUserDO tableUserDO) {
        this(tableUserDO, STRS_INCLUDE_PROPERTIES);
    }

    /**
     * 以实体类实例化 仅包含strsIncludeProperties的属性
     *
     * @param tableUserDO           实体类
     * @param strsIncludeProperties 包含的字段数组
     */
    public TableUserVO(final TableUserDO tableUserDO, final String[] strsIncludeProperties) {
        StrongUtils.copyProperties(tableUserDO, this, strsIncludeProperties);
    }

    /**
     * 主入口
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        String strClassName = Thread.currentThread().getStackTrace()[1].getClassName();
        CodeUtils.printTsModelCode(strClassName, STRS_INCLUDE_PROPERTIES);
    }

    // ===================扩展=======================

}
