package com.strong.sample.table.t_user.controller;

import cn.hutool.core.lang.Assert;
import com.strong.sample.table.t_user.TableUserConstants;
import com.strong.sample.table.t_user.jpa.TableUserDO;
import com.strong.sample.table.t_user.model.TableUserCreateDTO;
import com.strong.sample.table.t_user.model.TableUserRetrieveDTO;
import com.strong.sample.table.t_user.model.TableUserUpdateDTO;
import com.strong.sample.table.t_user.model.TableUserVO;
import com.strong.sample.table.t_user.service.TableUserService;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.ModuleJsonController;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.strong.utils.mvc.MvcConstants.PARAMETER_INTS_ID;
import static com.strong.utils.mvc.MvcConstants.PARAMETER_INT_ID;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_GENERAL;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_SUPER;

/**
 * JSON控制器
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Slf4j
@RestController
@RequestMapping(value = TableUserConstants.TABLE_ENTITY)
public class TableUserJsonController implements ModuleJsonController<TableUserVO, TableUserCreateDTO, TableUserUpdateDTO, TableUserRetrieveDTO> {

    /**
     * 过滤排序条件
     */
    private final static String[] STRS_FILTER_SORT = {};

    /**
     * 过滤查询条件
     */
    private final static String[] STRS_FILTER_SEARCH = {};

    /**
     * 注入Service
     */
    private final TableUserService tableUserService;

    /**
     * 实例化
     *
     * @param tableUserService 注入的Service
     */
    @Autowired
    public TableUserJsonController(TableUserService tableUserService) {
        this.tableUserService = tableUserService;
    }

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVO<TableUserVO> getRecordView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableUserDO tableUserDO = tableUserService.getRecord(intId);
        TableUserVO tableUserVO = tableUserService.getModel(tableUserDO);
        return new ReplyVO<>(tableUserVO);
    }

    /**
     * 添加记录操作 控制器
     *
     * @param modeldCreate 待添加的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUserVO> getCreateAction(@Valid @RequestBody final TableUserCreateDTO modeldCreate) {
        TableUserDO tableUserDO = tableUserService.getCreateAction(modeldCreate);
        TableUserVO tableUserVO = tableUserService.getModel(tableUserDO, TableUserVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(tableUserVO, ReplyEnum.SUCCESS_CREATE_DATA);
    }

    /**
     * 执行修改的 控制器.
     *
     * @param modelUpdate 待修改的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUserVO> getUpdateAction(@Valid @RequestBody final TableUserUpdateDTO modelUpdate) {
        TableUserDO tableUserDO = tableUserService.getUpdateAction(modelUpdate);
        TableUserVO tableUserVO = tableUserService.getModel(tableUserDO, TableUserVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(tableUserVO, ReplyEnum.SUCCESS_UPDATE_DATA);
    }

    /**
     * 显示待修改内容的 控制器.
     *
     * @param intId 显示对象的ID
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUserVO> getUpdateView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableUserDO tableUserDO = tableUserService.getUpdateRecord(intId);
        TableUserVO tableUserVO = tableUserService.getModel(tableUserDO);
        return new ReplyVO<>(tableUserVO);
    }

    /**
     * 执行删除的 控制器.
     *
     * @param intsId 待删除id数组
     * @return 基于无数据的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId) {
        tableUserService.getDeleteAction(intsId);
        return new ReplyVO<>(ReplyEnum.SUCCESS_DELETE_DATA);
    }

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型队列的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableUserVO> getListView(@RequestBody final TableUserRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        List<TableUserDO> listTableUserDO = tableUserService.getAllList(modelRetrieve);
        List<TableUserVO> listTableUserVO = tableUserService.getModelList(listTableUserDO, TableUserVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableUserVO);
    }

    /**
     * 显示分页列表的 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型分页对象的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableUserVO> getPageView(@RequestBody final TableUserRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        Page<TableUserDO> pageTableUserDO = tableUserService.getPageList(modelRetrieve);
        List<TableUserVO> listTableUserVO = tableUserService.getModelList(pageTableUserDO.getContent(), TableUserVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableUserVO, pageTableUserDO.getTotalElements());
    }


    // ===================扩展=======================
}
