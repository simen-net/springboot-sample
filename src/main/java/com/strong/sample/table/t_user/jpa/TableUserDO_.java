package com.strong.sample.table.t_user.jpa;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;

/**
 * 托管类
 *
 * @author Simen
 * @date 2024-10-22 10:21:39
 */
@StaticMetamodel(TableUserDO.class)
public class TableUserDO_ {
    /**
     * 系统随机生成固定编号
     */
    public static volatile SingularAttribute<TableUserDO, Integer> userId;

    /**
     * 用户名
     */
    public static volatile SingularAttribute<TableUserDO, String> userName;

    /**
     * 用户登录名
     */
    public static volatile SingularAttribute<TableUserDO, String> userLoginName;

    /**
     * 用户登录密码
     */
    public static volatile SingularAttribute<TableUserDO, String> userLoginPassword;

    /**
     * 用户登录密码加盐
     */
    public static volatile SingularAttribute<TableUserDO, String> userLoginSalt;

    /**
     * 用户权限
     */
    public static volatile SingularAttribute<TableUserDO, String> userAuthority;

    /**
     * 用户分组
     */
    public static volatile SingularAttribute<TableUserDO, String> userGroup;

    /**
     * [外键]对应运动队编号-关联T_TEAM外键
     */
    public static volatile SingularAttribute<TableUserDO, String> userTeamOrgId;

    /**
     * [外键]用户所属单位-关联单位表主键
     */
    public static volatile SingularAttribute<TableUserDO, Integer> userUnitId;

    /**
     * 证件类型
     */
    public static volatile SingularAttribute<TableUserDO, String> userIdType;

    /**
     * 证件号码
     */
    public static volatile SingularAttribute<TableUserDO, String> userIdNumber;

    /**
     * 用户手机号码
     */
    public static volatile SingularAttribute<TableUserDO, String> userMobilePhone;

    /**
     * 用户固定电话
     */
    public static volatile SingularAttribute<TableUserDO, String> userTelephone;

    /**
     * 用户邮箱地址
     */
    public static volatile SingularAttribute<TableUserDO, String> userEmail;

    /**
     * 用户QQ号码
     */
    public static volatile SingularAttribute<TableUserDO, String> userQqCode;

    /**
     * 用户微信号码
     */
    public static volatile SingularAttribute<TableUserDO, String> userWxCode;

    /**
     * 用户登录记录
     */
    public static volatile SingularAttribute<TableUserDO, String> userLoginRecord;

}