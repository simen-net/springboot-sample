package com.strong.system.constroller;

import com.strong.utils.exception.StrongRuntimeException;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.strong.utils.security.AuthorityConstants.*;

/**
 * 系统控制器
 *
 * @author simen
 * @date 2022/02/17
 */
@Slf4j
@RestController
public class SystemJsonController {
    /**
     * URL 公开获取map
     */
    public static final String URL_PERSON_MAP_JSON = "/getPersonMapJson";

    /**
     * URL 读取json
     */
    public static final String URL_PERSON_JSON_READ = "/getPersonJsonRead";

    /**
     * URL 写入json
     */
    public static final String URL_PERSON_JSON_WRITE = "/getPersonJsonWrite";

    /**
     * URL 读取并写入json
     */
    public static final String URL_PERSON_JSON_READ_AND_WRITE = "/getPersonJsonReadAndWrite";

    /**
     * URL 抛出异常的URL
     */
    public static final String URL_PERSON_JSON_ERROR = "/getPersonJsonError";

    /**
     * Url 完整正常访问的数组
     */
    public static final String[] URLS_TEST_200 = {URL_PERSON_MAP_JSON, URL_PERSON_JSON_READ, URL_PERSON_JSON_WRITE, URL_PERSON_JSON_READ_AND_WRITE};

    /**
     * Url用户测试测试数组
     */
    public static final String[] URLS_ANONYMOUS_TEST = {URL_PERSON_MAP_JSON};

    /**
     * Url用户测试测试数组
     */
    public static final String[] URLS_USER_TEST = {URL_PERSON_JSON_READ, URL_PERSON_JSON_READ_AND_WRITE, URL_PERSON_MAP_JSON};

    /**
     * Url管理测试测试数组
     */
    public static final String[] URLS_ADMIN_TEST = {URL_PERSON_MAP_JSON, URL_PERSON_JSON_READ, URL_PERSON_JSON_WRITE, URL_PERSON_JSON_READ_AND_WRITE};

    /**
     * 获取测试map 匿名权限
     *
     * @return {@link ReplyVO }<{@link String }>
     */
    @RequestMapping(URL_PERSON_MAP_JSON)
    public ReplyVO<String> getPersonMapJson() {
        ReplyVO<String> replyVO = new ReplyVO<>();
        replyVO.setCode(200);
        replyVO.setMsg(URL_PERSON_MAP_JSON);
        replyVO.setData(URL_PERSON_MAP_JSON);
        return replyVO;
    }

    /**
     * 读取人员操作 读权限
     *
     * @return {@link ReplyVO }<{@link String }>
     */
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    @RequestMapping(URL_PERSON_JSON_READ)
    public ReplyVO<String> getPersonJsonRead() {
        ReplyVO<String> replyVO = new ReplyVO<>();
        replyVO.setCode(200);
        replyVO.setMsg(URL_PERSON_JSON_READ);
        replyVO.setData(URL_PERSON_JSON_READ);
        return replyVO;
    }

    /**
     * 写人员操作 写权限
     *
     * @return {@link ReplyVO }<{@link String }>
     */
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    @RequestMapping(URL_PERSON_JSON_WRITE)
    public ReplyVO<String> getPersonJsonWrite() {
        ReplyVO<String> replyVO = new ReplyVO<>();
        replyVO.setCode(200);
        replyVO.setMsg(URL_PERSON_JSON_WRITE);
        replyVO.setData(URL_PERSON_JSON_WRITE);
        return replyVO;
    }

    /**
     * 读写人员操作 读写权限
     *
     * @return {@link ReplyVO }<{@link String }>
     */
    @PreAuthorize("hasAnyAuthority('" + STR_AUTHORITY_SUPER + "', '" + STR_AUTHORITY_GENERAL + "')")
    @RequestMapping(URL_PERSON_JSON_READ_AND_WRITE)
    public ReplyVO<String> getPersonJsonReadAndWrite() {
        ReplyVO<String> replyVO = new ReplyVO<>();
        replyVO.setCode(200);
        replyVO.setMsg(URL_PERSON_JSON_READ_AND_WRITE);
        replyVO.setData(URL_PERSON_JSON_READ_AND_WRITE);
        return replyVO;
    }

    /**
     * 抛出异常
     *
     * @return {@link ReplyVO }<{@link String }>
     * @throws StrongRuntimeException 强运行时异常
     */
    @RequestMapping(URL_PERSON_JSON_ERROR)
    public ReplyVO<String> getPersonJsonError() throws StrongRuntimeException {
        throw new StrongRuntimeException(String.format("系统抛出错误%s", URL_PERSON_JSON_ERROR));
    }
}
